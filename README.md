SmartRover_App
==============

기존에 동작하던 버전이 Android 10 에서 동작하지 않아서 소스를 수정하였습니다.
수정 사항은 

1. eclipse 기반의 빌드 환경을 android studio 로 변경하였습니다.
2. bluetooth scan이 동작하지 않던 부분을 동작되도록 수정하였습니다. 

